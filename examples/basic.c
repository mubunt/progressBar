//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libProgressBar
// C library to display a progress bar to show a user how far along he is in a process.
//------------------------------------------------------------------------------
#include <stdio.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include "progressbar.h"
//------------------------------------------------------------------------------
// TESTS
//------------------------------------------------------------------------------
#define TEMPO				1
static void determinate(const char *id, const char *label, enum eprogressbar_style style, enum eprogressbar_location loc) {
	fprintf(stdout, "	Test #%s - %s\n", id, label);
	fprintf(stdout, "		Test #%s.1  - %s - 0%%\n", id, label);
	progressbar_init(DETERMINED, style, loc);
	sleep(TEMPO);
	for (int i = 1; i < 11; i++) {
		fprintf(stdout, "		Test #%s.%-2d - %s - %d%%\n", id, i + 1, label, i * 10);
		progressbar_display(i * 10);
		sleep(TEMPO);
	}
	progressbar_close();
}
static void indeterminate(const char *id, const char *label, enum eprogressbar_style style, enum eprogressbar_location loc) {
	fprintf(stdout, "	Test #%s - %s\n", id, label);
	progressbar_init(INDETERMINED, style, loc);
	for (int k = 1; k <= 20; k++) {
		fprintf(stdout, ".");
		fflush(stdout);
		sleep(TEMPO);
	}
	fprintf(stdout, "\n");
	progressbar_close();
}

int main( void ) {
	// Identification
	char version[128];
	progressbar_version(version, sizeof(version));
	fprintf(stdout, "%s\n\n", version);

	// Tests
	fprintf(stdout, "Tests #1 - TEXT PROGRESS BAR\n");
	determinate("1.1", "Text progress bar at the bottom", TEXT, BOTTOM);
	determinate("1.2", "Text progress bar at the top", TEXT, TOP);

	fprintf(stdout, "Tests #2 - SIMPLE PROGRESS BAR\n");
	determinate("2.1", "Simple progress bar at the bottom", SIMPLE, BOTTOM);
	determinate("2.2", "Simple progress bar at the top", SIMPLE, TOP);

	fprintf(stdout, "Tests #3 - FRAMED PROGRESS BAR\n");
	determinate("3.1", "Framed progress bar at the bottom", FRAMED, BOTTOM);
	determinate("3.2", "Framed progress bar at the top", FRAMED, TOP);

	fprintf(stdout, "Tests #4 - INDETERMINATE TEXT PROGRESS BARE\n");
	indeterminate("4.1", "Indeterminate text progress bar at the bottom", TEXT, BOTTOM);
	indeterminate("4.2", "Indeterminate text progress bar at the top", TEXT, TOP);

	fprintf(stdout, "Tests #5 - INDETERMINATE SIMPLE PROGRESS BARE\n");
	indeterminate("5.1", "Indeterminate simple progress bar at the bottom", SIMPLE, BOTTOM);
	indeterminate("5.2", "Indeterminate simple progress bar at the top", SIMPLE, TOP);

	fprintf(stdout, "Tests #6 - INDETERMINATE FRAMED PROGRESS BARE\n");
	indeterminate("6.1", "Indeterminate framed progress bar at the bottom", FRAMED, BOTTOM);
	indeterminate("6.2", "Indeterminate framed progress bar at the top", FRAMED, TOP);

	fprintf(stdout, "Tests #7 - USE CASE\n");
	progressbar_init(DETERMINED, SIMPLE, BOTTOM);
	progressbar_color(RED);
	for (int k = 1; k <= 100; k++) {
		fprintf(stdout, ".");
		fflush(stdout);
		progressbar_display(k);
		sleep(TEMPO);
	}
	progressbar_close();
	fprintf(stdout, "\n");
	// Exit
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
