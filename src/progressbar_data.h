//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libProgressBar
// C library to display a progress bar to show a user how far along he is in a process.
//------------------------------------------------------------------------------
#ifndef PROGRESSBAR_DATA_H
#define PROGRESSBAR_DATA_H
//------------------------------------------------------------------------------
// ADDITIONAL SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <sys/ioctl.h>
#include <termios.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define cprogressbar_BACKGROUND_COLOR						"\033[1;%dm"
#define cprogressbar_FOREGROUND_RED							"\033[1;31m"
#define cprogressbar_STOP_COLOR								"\033[0m"
#define cprogressbar_XSRBC									"╯"
#define cprogressbar_XSRUC									"┐"
#define cprogressbar_XSLUC									"┌"
#define cprogressbar_XSLBC									"└"
#define	cprogressbar_XSH									"─"
#define cprogressbar_XSV									"│"

#define cprogressbar_FRAMEDLIGHTSHADE						"░"
#define cprogressbar_FRAMEDBLOCK							"█"
#define cprogressbar_FRAMEDBAR								"██████████"

#define cprogressbar_SIMPLELIGHTSHADE						"░"
#define cprogressbar_SIMPLEBLOCK							' '
#define cprogressbar_SIMPLEBAR								"          "

#define cprogressbar_TEXTLIGHTSHADE							' '
#define cprogressbar_TEXTLIGHTSHADEIND						' '
#define cprogressbar_TEXTBLOCK								'-'
#define cprogressbar_TEXTBAR								"##########"
#define cprogressbar_TEXTEND								'>'
#define cprogressbar_TEXTSIDE								'|'

#define cprogressbar_DISPLAY(...)							fprintf(stdout,  __VA_ARGS__)
#define cprogressbar_FLUSH									fflush(stdout)
#define CHECK_IF_PROGRESSBAR_IS_DEFINED						if (vprogressbar_style == UNDEFINED) \
																progressbar_error("%s", "Unconsistent call to 'display' or 'close' routine. Abort!")
#define CHECK_IF_PROGRESSBAR_IS_NOT_DEFINED					if (vprogressbar_style != UNDEFINED) \
																progressbar_error("%s", "Multiple calls to 'init' routine. Abort!")
#define CHECK_IF_STDINOUT_REFER_TO_A_TERMINAL				if (! isatty(fileno(stdout)) || ! isatty(fileno(stdin))) \
																progressbar_error("%s", "stdout or stdin does not refer to a terminal. Abort!")

// To migrate to portable mechanism (terminfo, tputs, ...) later!!!
#define MOVE_CURSOR(x, y)									cprogressbar_DISPLAY("\033[%d;%dH", x, y)
#define SET_TOP_AND_BOTTOM_OF_THE_SCROLLABLE_WINDOW(t, b)	cprogressbar_DISPLAY("\033[%d;%dr", t, b)
#define SCROLL_WINDOW_UP									cprogressbar_DISPLAY("\033D")
#define MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE(x, y)				cprogressbar_DISPLAY("\033[%d;%dH\033[2K", x, y)
#define REQUEST_CURSOR_POSITION								cprogressbar_DISPLAY("\033[6n"); cprogressbar_FLUSH

#define cprogressbar_THREADTEMPO							500	// milliseconds
#define cprogressbar_ERRORPREFIX							"PROGRESS BAR:"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
extern int vprogressbar_hundredpercent;
extern int vprogressbar_nbLINES;
extern int vprogressbar_x;
extern int vprogressbar_lines;
extern pthread_t vprogressbar_thread;
extern pthread_mutex_t vprogressbar_mutex;
extern enum eprogressbar_style vprogressbar_style;
extern enum eprogressbar_location vprogressbar_location;
extern enum eprogressbar_color vprogressbar_color;
extern enum eprogressbar_type vprogressbar_type;
//------------------------------------------------------------------------------
// EXTERNAL ROUTINES
//------------------------------------------------------------------------------
extern void *progressbar_thread( void * );
extern void progressbar_decoration( int );
extern void progressbar_getcursor( int *, int * );
extern void progressbar_error(const char *, ...);
//------------------------------------------------------------------------------
#endif	// PROGRESSBAR_DATA_H
