//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libProgressBar
// C library to display a progress bar to show a user how far along he is in a process.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include "progressbar.h"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
// Identification
#define PROGRESSBAR_PACKAGE		"progressbar library"
#ifdef VERSION
#define PROGRESSBAR_VERSION(x) 	str(x)
#define str(x)					#x
#else
#define PROGRESSBAR_VERSION 	"Unknown"
#endif
//------------------------------------------------------------------------------
// MAIN ROUTINE
//------------------------------------------------------------------------------
void progressbar_version( char *version, size_t len ) {
	snprintf(version, len, "%s - Version %s", PROGRESSBAR_PACKAGE, PROGRESSBAR_VERSION(VERSION));
}
//------------------------------------------------------------------------------
