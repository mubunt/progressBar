//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libProgressBar
// C library to display a progress bar to show a user how far along he is in a process.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include "progressbar.h"
#include "progressbar_data.h"
//------------------------------------------------------------------------------
// INTERNAL ROUTINES
//------------------------------------------------------------------------------
static void sleep_ms( int milliseconds ) {
	struct timespec ts;
	ts.tv_sec = milliseconds / 1000;
	ts.tv_nsec = (milliseconds % 1000) * 1000000;
	nanosleep(&ts, NULL);
}
//------------------------------------------------------------------------------
// MAIN ROUTINE
//------------------------------------------------------------------------------
void *progressbar_thread( void *arg ) {
	int i, line, col, currentpercent = 0;
	char *partialbloc;

	switch (vprogressbar_style) {
	case FRAMED:
		partialbloc = malloc(strlen(cprogressbar_FRAMEDBAR) + 1);
		strcpy(partialbloc, cprogressbar_FRAMEDBAR);
		partialbloc[3 * (vprogressbar_hundredpercent % (int)strlen(partialbloc))] = '\0';
		break;
	case SIMPLE:
		partialbloc = malloc(strlen(cprogressbar_SIMPLEBAR) + 1);
		strcpy(partialbloc, cprogressbar_SIMPLEBAR);
		partialbloc[vprogressbar_hundredpercent % (int)strlen(partialbloc)] = '\0';
		break;
	case TEXT:
		partialbloc = malloc(strlen(cprogressbar_TEXTBAR) + 1);
		strcpy(partialbloc, cprogressbar_TEXTBAR);
		partialbloc[vprogressbar_hundredpercent % (int)strlen(partialbloc)] = '\0';
		break;
	default:
		break;
	}

	while (1) {
		pthread_mutex_lock(& vprogressbar_mutex);
		progressbar_getcursor(&line, &col);
		pthread_mutex_unlock(& vprogressbar_mutex);
		switch (vprogressbar_style) {
		case FRAMED:
			// WARNING:  a block character string of n elements has a length of 3 * n !!!
			MOVE_CURSOR(vprogressbar_x + 1, 1);
			cprogressbar_DISPLAY("%s", cprogressbar_XSV);
			for (i = 0; i < currentpercent; i++)
				cprogressbar_DISPLAY("%s", cprogressbar_FRAMEDLIGHTSHADE);
			if (((int)strlen(cprogressbar_FRAMEDBAR)) > vprogressbar_hundredpercent - currentpercent) {
				//partialbloc[3 * (vprogressbar_hundredpercent - currentpercent)] = '\0';
				cprogressbar_DISPLAY("%s", partialbloc);
			} else
				cprogressbar_DISPLAY("%s", cprogressbar_FRAMEDBAR);
			for (i = currentpercent + ((int) strlen(cprogressbar_FRAMEDBAR) / 3); i < vprogressbar_hundredpercent; i++)
				cprogressbar_DISPLAY("%s", cprogressbar_FRAMEDLIGHTSHADE);
			cprogressbar_DISPLAY("%s", cprogressbar_XSV);
			currentpercent += (int)strlen(cprogressbar_FRAMEDBAR);
			break;
		case SIMPLE:
			MOVE_CURSOR(vprogressbar_x, 1);
			for (i = 0; i < currentpercent; i++)
				cprogressbar_DISPLAY("%s", cprogressbar_SIMPLELIGHTSHADE);
			cprogressbar_DISPLAY(cprogressbar_BACKGROUND_COLOR, 40 + vprogressbar_color);
			if (((int)strlen(cprogressbar_SIMPLEBAR)) > vprogressbar_hundredpercent - currentpercent) {
				//partialbloc[vprogressbar_hundredpercent - currentpercent] = '\0';
				cprogressbar_DISPLAY("%s", partialbloc);
			} else
				cprogressbar_DISPLAY("%s", cprogressbar_SIMPLEBAR);
			cprogressbar_DISPLAY("%s", cprogressbar_STOP_COLOR);
			for (i = currentpercent + (int) strlen(cprogressbar_SIMPLEBAR); i < vprogressbar_hundredpercent; i++)
				cprogressbar_DISPLAY("%s", cprogressbar_SIMPLELIGHTSHADE);
			currentpercent += (int)strlen(cprogressbar_SIMPLEBAR);
			break;
		case TEXT:
			MOVE_CURSOR(vprogressbar_x, 1);
			cprogressbar_DISPLAY("%c", cprogressbar_TEXTSIDE);
			for (i = 0; i < currentpercent; i++)
				cprogressbar_DISPLAY("%c", cprogressbar_TEXTLIGHTSHADEIND);
			if (((int)strlen(cprogressbar_TEXTBAR)) > vprogressbar_hundredpercent - currentpercent) {
				//partialbloc[vprogressbar_hundredpercent - currentpercent] = '\0';
				cprogressbar_DISPLAY("%s", partialbloc);
			} else
				cprogressbar_DISPLAY("%s", cprogressbar_TEXTBAR);
			for (i = currentpercent + (int) strlen(cprogressbar_TEXTBAR); i < vprogressbar_hundredpercent; i++)
				cprogressbar_DISPLAY("%c", cprogressbar_TEXTLIGHTSHADEIND);
			cprogressbar_DISPLAY("%c",cprogressbar_TEXTSIDE);
			currentpercent += (int)strlen(cprogressbar_TEXTBAR);
			break;
		default:
			break;
		}
		if (currentpercent > vprogressbar_hundredpercent) currentpercent = 0;
		MOVE_CURSOR(line, col);
		cprogressbar_FLUSH;
		sleep_ms(cprogressbar_THREADTEMPO);
	}
	// Never run ....
	free(partialbloc);
	pthread_exit(NULL);
	return NULL;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void progressbar_decoration( int percent ) {
	int line, col;
	progressbar_getcursor(&line, &col);
	switch (vprogressbar_location) {
	case TOP:
		if (line < vprogressbar_lines) line = vprogressbar_lines + 1;
		for (int i = 0; i < vprogressbar_lines; i++)
			MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE(vprogressbar_x + i, 1);
		break;
	default:
	case BOTTOM:
		if (line >= vprogressbar_x) {
			MOVE_CURSOR(vprogressbar_nbLINES, 1);
			for (int i = 0; i < vprogressbar_lines; i++) SCROLL_WINDOW_UP;
			if (line > vprogressbar_lines) line = line - vprogressbar_lines;
		}
		break;
	}
	switch (vprogressbar_style) {
	case FRAMED:
		MOVE_CURSOR(vprogressbar_x, 1);
		switch (vprogressbar_type) {
		case DETERMINED:
			cprogressbar_DISPLAY("     %s", cprogressbar_XSLUC);
			for (int i = 0; i < vprogressbar_hundredpercent; i++) cprogressbar_DISPLAY("%s", cprogressbar_XSH);
			cprogressbar_DISPLAY("%s", cprogressbar_XSRUC);
			MOVE_CURSOR(vprogressbar_x + 2, 1);
			cprogressbar_DISPLAY("     %s", cprogressbar_XSLBC);
			for (int i = 0; i < vprogressbar_hundredpercent; i++) cprogressbar_DISPLAY("%s", cprogressbar_XSH);
			cprogressbar_DISPLAY("%s", cprogressbar_XSRBC);
			progressbar_display(percent);
			break;
		case INDETERMINED:
			cprogressbar_DISPLAY("%s", cprogressbar_XSLUC);
			for (int i = 0; i < vprogressbar_hundredpercent; i++) cprogressbar_DISPLAY("%s", cprogressbar_XSH);
			cprogressbar_DISPLAY("%s", cprogressbar_XSRUC);
			MOVE_CURSOR(vprogressbar_x + 2, 1);
			cprogressbar_DISPLAY("%s", cprogressbar_XSLBC);
			for (int i = 0; i < vprogressbar_hundredpercent; i++) cprogressbar_DISPLAY("%s", cprogressbar_XSH);
			cprogressbar_DISPLAY("%s", cprogressbar_XSRBC);
			pthread_create(&vprogressbar_thread, NULL, progressbar_thread, (char *)"");	// start display thread
			break;
		}
		break;
	case SIMPLE:
	case TEXT:
		switch (vprogressbar_type) {
		case DETERMINED:
			progressbar_display(percent);
			break;
		case INDETERMINED:
			pthread_create(&vprogressbar_thread, NULL, progressbar_thread, (char *)"");	// start display thread
			break;
		}
		break;
	default:
		break;
	}
	switch (vprogressbar_location) {
	case TOP:
		SET_TOP_AND_BOTTOM_OF_THE_SCROLLABLE_WINDOW(vprogressbar_x + vprogressbar_lines, vprogressbar_nbLINES);
		break;
	default:
	case BOTTOM:
		SET_TOP_AND_BOTTOM_OF_THE_SCROLLABLE_WINDOW(1, vprogressbar_x - 1);
		break;
	}
	MOVE_CURSOR(line, col);
	cprogressbar_FLUSH;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void progressbar_display( int percent ) {
	CHECK_IF_PROGRESSBAR_IS_DEFINED;
	if (percent < 0) percent = 0;
	if (percent > 100) percent = 100;
	int currentpercent = vprogressbar_hundredpercent * percent / 100;

	int i, line, col;
	progressbar_getcursor(&line, &col);
	switch (vprogressbar_style) {
	case FRAMED:
		MOVE_CURSOR(vprogressbar_x + 1, 1);
		cprogressbar_DISPLAY("%3d%% %s", percent, cprogressbar_XSV);
		for (i = 0; i < currentpercent; i++) cprogressbar_DISPLAY("%s", cprogressbar_FRAMEDBLOCK);
		for (i = currentpercent; i < vprogressbar_hundredpercent; i++) cprogressbar_DISPLAY("%s", cprogressbar_FRAMEDLIGHTSHADE);
		cprogressbar_DISPLAY("%s",cprogressbar_XSV);
		break;
	case SIMPLE:
		MOVE_CURSOR(vprogressbar_x, 1);
		cprogressbar_DISPLAY("%3d%% " cprogressbar_BACKGROUND_COLOR, percent, 40 + vprogressbar_color);
		for (i = 0; i < currentpercent; i++) cprogressbar_DISPLAY("%c", cprogressbar_SIMPLEBLOCK);
		cprogressbar_DISPLAY("%s", cprogressbar_STOP_COLOR);
		for (i = currentpercent; i < vprogressbar_hundredpercent; i++) cprogressbar_DISPLAY("%s", cprogressbar_SIMPLELIGHTSHADE);
		break;
	case TEXT:
		MOVE_CURSOR(vprogressbar_x, 1);
		cprogressbar_DISPLAY("%3d%% %c", percent, cprogressbar_TEXTSIDE);
		for (i = 0; i < currentpercent - 1; i++) cprogressbar_DISPLAY("%c", cprogressbar_TEXTBLOCK);
		if (currentpercent != 0) cprogressbar_DISPLAY("%c", cprogressbar_TEXTEND);
		for (i = currentpercent; i < vprogressbar_hundredpercent; i++) cprogressbar_DISPLAY("%c", cprogressbar_TEXTLIGHTSHADE);
		cprogressbar_DISPLAY("%c",cprogressbar_TEXTSIDE);
		break;
	default:
		break;
	}
	switch (vprogressbar_location) {
	case TOP:
		SET_TOP_AND_BOTTOM_OF_THE_SCROLLABLE_WINDOW(vprogressbar_x + vprogressbar_lines, vprogressbar_nbLINES);
		break;
	default:
	case BOTTOM:
		SET_TOP_AND_BOTTOM_OF_THE_SCROLLABLE_WINDOW(1, vprogressbar_x - 1);
		break;
	}
	MOVE_CURSOR(line, col);
	cprogressbar_FLUSH;
}
//------------------------------------------------------------------------------
