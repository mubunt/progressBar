//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libProgressBar
// C library to display a progress bar to show a user how far along he is in a process.
//------------------------------------------------------------------------------
#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//------------------------------------------------------------------------------
// ENUMS DEFINITIONS
//------------------------------------------------------------------------------
enum eprogressbar_location { BOTTOM, TOP };
enum eprogressbar_style { UNDEFINED, FRAMED, SIMPLE, TEXT };
enum eprogressbar_color { BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE };
enum eprogressbar_type { DETERMINED, INDETERMINED };
//------------------------------------------------------------------------------
// ROUTINES DEFINITIONS
//------------------------------------------------------------------------------
extern void progressbar_init( enum eprogressbar_type, enum eprogressbar_style, enum eprogressbar_location);
extern void progressbar_display( int );
extern void progressbar_indeterminate( void );
extern void progressbar_close( void );
extern void progressbar_color( enum eprogressbar_color );
extern void progressbar_version( char *, size_t );
//------------------------------------------------------------------------------
#endif	// PROGRESSBAR_H
