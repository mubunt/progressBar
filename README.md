# *libProgressBar*, a C library for LINUX to display a xterm-like progress bar
***
To convey how complete a task is, you can use a progress bar like this one:

![libProgressBar](README_images/2.png  "Determined progress bar")

Sometimes you can't immediately determine the length of a long-running task, or the task might stay stuck at the same state of completion for a long time. You can show work without measurable progress by putting the progress bar in indeterminate mode. A progress bar in indeterminate mode displays animation to indicate that work is occurring. As soon as the progress bar can display more meaningful information, you should switch it back into its default, determinate mode. Indeterminate progress bars look like this:

![libProgressBar](README_images/5.png  "Indetermined progress bar")
## LICENSE
**libProgressBar** is covered by the GNU General Public License (GPL) version 3and above.
## API DEFINITION
### progressbar_init
Synopsys:
```C
#include "progressbar.h"
void progressbar_init( enum eprogressbar_type Type, enum eprogressbar_style Style, enum eprogressbar_location Location);
```
Description: Creates a new progress bar with range 0...100 and initial progress of 0. *Type* specifies the type of the progress bar (**DETERMINED** or **INDETERMINED**). *Style* specifies the display style of the progress bar (**TEXT**,  **SIMPLE** or **FRAMED**). *Location* specifies the location the progress bar in the terminal (**TOP** or **BOTTOM**).


If *Location* =**TOP**, the progress bar will be located at the top of the terminal and lines needed for this display are lost.
If *Location* = **BOTTOM**, the progress bar will be located at the bottm of the terminal and to avoid losing the last displayed lines, the space required to display the progress bar is obtained by scrolling all the lines upwards.


If *Type* = **DETERMINED** and  *Style* = **TEXT**

![libProgressBar](README_images/1.png  "Type=DETERMINED, Style=TEXT")

If *Type* = **DETERMINED** and  *Style* = **SIMPLE**

![libProgressBar](README_images/2.png  "Type=DETERMINED, Style=SIMPLE")

If *Type* = **DETERMINED** and  *Style* = **FRAMED**

![libProgressBar](README_images/3.png  "Type=DETERMINED, Style=FRAMED")

If *Type* = **INDETERMINED** and  *Style* = **TEXT**

![libProgressBar](README_images/4.png  "Type=INDETERMINED, Styl =TEXT")

If *Type* = **INDETERMINED** and  *Style* = **SIMPLE**

![libProgressBar](README_images/5.png  "Type=INDETERMINED, Style=SIMPLE")

If *Type* = **INDETERMINED** and  *Style* = **FRAMED**

![libProgressBar](README_images/6.png  "Type=INDETERMINED, Style=FRAMED")

### progressbar_display
Synopsys:
```C
#include "progressbar.h"
void progressbar_display( int percent );
```
Description: Sets the state of the bar to reflect the progress when the determined mode is used. To be used with a **DETERMINED** progress bar.
### progressbar_indeterminate
Synopsys:
```C
#include "progressbar.h"
void progressbar_indeterminate( void );
```
Description: Runs the animation to indicate that work is occurring. To be used with a **INDETERMINED** progress bar.
### progressbar_close
Synopsys:
```C
#include "progressbar.h"
void progressbar_close( void );
```
Description: Removes the progress bar.
### progressbar_color
Synopsys:
```C
#include "progressbar.h"
void progressbar_color( enum eprogressbar_color color);
```
Description: This function defines the color of the progress bar. By default, the progress bar is WHITE. Allowed colors are: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN and WHITE.

![progressBar](README_images/7.png  "Progress=DETERMINED, *Style* = SIMPLE, Color=RED")
### progressbar_version
Synopsys:
```C
#include "progressbar.h"
void progressbar_version( char *version, size_t length);
```
Description: This function copies the version identification of the **progressbar** library, including the terminating null byte ('\0'), to the buffer pointed to by *version*. *length* is the size of this buffer.
The format of the version identification string is:

    progressbar library - Version R.L.f
## EXAMPLES
### Determined progress bar
```C
progressbar_init(DETERMINED, SIMPLE, BOTTOM);
progress_color(RED)
sleep(TEMPO);
for (int i = 1; i < 11; i++) {
	fprintf(stdout, "Percentage: %d%%\n", i * 10);
	progressbar_display(i * 10);
	sleep(1);
}
progressbar_close();
```
### Indetermined progress bar
```C
progressbar_init(INDETERMINED, SIMPLE, BOTTOM);
for (int k = 1; k <= 20; k++) {
	fprintf(stdout, ".");
	fflush(stdout);
	sleep(1);
}
fprintf(stdout, "\n");
	progressbar_close();
```
## STRUCTURE OF THE APPLICATION
This section walks you through **libProgressBar**'s structure. Once you understand this structure, you will easily find your way around in **libProgressBar**'s code base.
``` bash
$ yaTree
./                              # Application level
├── README_images/              # Images for documentation
│   ├── 1.png                   # 
│   ├── 2.png                   # 
│   ├── 3.png                   # 
│   ├── 4.png                   # 
│   ├── 5.png                   # 
│   ├── 6.png                   # 
│   └── 7.png                   # 
├── examples/                   # 
│   ├── Makefile                # Makefile
│   └── basic.c                 # Example of 'progressbar' usage
├── src/                        # Source directory
│   ├── Makefile                # Makefile
│   ├── progressbar.h           # Application header file
│   ├── progressbar_close.c     # progressbar_close implementation
│   ├── progressbar_color.c     # progressbar_color implementation
│   ├── progressbar_data.h      # Internal header file
│   ├── progressbar_display.c   # progressbar_display routine implementation
│   ├── progressbar_init.c      # progressbar_init implementation
│   ├── progressbar_init.c.orig # 
│   └── progressbar_version.c   # progressbar_version implementation
├── COPYING.md                  # GNU General Public License markdown file
├── LICENSE.md                  # License markdown file
├── Makefile                    # Makefile
├── README.md                   # ReadMe Mark-Down file
├── RELEASENOTES.md             # Release Notes markdown file
└── VERSION                     # Version identification text file

3 directories, 24 files
```

## HOW TO BUILD THIS APPLICATION IN DEBUG MODE
```Shell
$ cd libProgressBar
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION IN RELEASE MODE
```Shell
$ cd libProgressBar
$ make release
    # Library generated with -O2 option and the header file, are respectively installed in $LIB_DIR and $INC_DIR directories (defined at environment level).
```

## HOW TO BUILD AND RUN TEST
```Shell
$ cd libProgressBar
$ make all
$ ./linux/progressbar_example
```

## NOTES
- The  header  file  <progressbar.h>  automatically  includes the header files <stdio.h>, <stdlib.h> and <unistd.h>
- If standard output is re-directed  to  something which  is not a tty, an error is printed onstandard error and the program aborts.
- Application woks fine with *xterm*, *uxtem* and *terminator*; some troubles with *tilix*.
- Application developed and tested with gcc 7.3.0 running on XUBUNTU 18.04.

## SOFTWARE REQUIREMENTS
- For development: Nothing particular...
- For usage: *pthread* library is to be linked with the executable using **progressbar** (option *-lpthread*).

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES.md) .
***
