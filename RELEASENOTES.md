# RELEASE NOTES: *libProgressBar*, a library to display a progress bar to show a user how far along he is in a process.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.1.9**:
  - Updated build system components.

- **Version 1.1.8**:
  - Updated build system.

- **Version 1.1.7**:
  - Removed unused files.

- **Version 1.1.6**:
  - Updated build system component(s)

- **Version 1.1.5**:
  - Renamed executable built as example as *example_xxx* to be compliant with other projects.

- **Version 1.1.4**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.1.3**:
  - Some minor changes in .comment file(s).

- **Version 1.1.2**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.1.1**:
  - Updated makefiles and asociated README information.

- **Version 1.1.0**:
  - Renamed project as **libProgressBar**.

- **Version 1.0.8**:
  - Fixed ./Makefile and ./src/Makefile.

- **Version 1.0.7**:
  - Added tagging of new release.

- **Version 1.0.6**:
  - Improved Makefiles: version identification, optimization options (debug, release).
  - Added version identification text file.
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Moved from GPL v2 to GPL v3.

- **Version 1.0.5**:
  - Fixed typos in README.md file.

- **Version 1.0.4**:
  - Updated src/Makefile
  - Removed compilation warning (progressbar_init.c)
  - Simplified display of graphical characters in the terminal.

- **Version 1.0.3**:
  - Fixed src/Makefile: delivering and removing of header file in install and cleaninstall targets.
  - Added requirement information un README file.

- **Version 1.0.2**:
  - Added COPYING (GNU General Public License) file.

- **Version 1.0.1**:
  - Formatting and improving the README file.
  - Adding LICENSE file.

- **Version 1.0.0**:
  - First release.
